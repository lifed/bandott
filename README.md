## 便當屋(前端)
網頁樣板來源：[Foody](https://themewagon.com/themes/free-bootstrap-5-html5-organic-food-website-template-foody/)

[示範頁面](https://bandott365.gitlab.io/bandott)
目前示範頁面與家用後台結合中，若看到『後台伺服器尚未啟動』，表示目前後台未開機。

### 安裝模組
```
npm install
```

### 執行Server
```
npm run serve
```

### 更新
#### npm
更新NPM之前或許您需要先更新Node
```
npm install -g npm
```
#### node
請搜尋官方網站下載新版本。

#### npm-check-updates
[npm-check-updates](https://www.npmjs.com/package/npm-check-updates)
```
ncu -u
npm install
```


