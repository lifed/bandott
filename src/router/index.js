import {createRouter, createWebHistory} from 'vue-router'
import LayoutIndex from '@/Layout/index'
import store from '@/store'
import {UserRoute} from "@/router/UserRoute";
import {OrderRoute} from "@/router/OrderRoute";
import {StoreRoute} from "@/router/StoreRoute";

const IndexRoute = [
    {
        path: '/',
        name: 'home',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/HomeView.vue'),
                name: 'dashboard',
                meta: {
                    title: '便當屋',
                    // showHeader: false
                }
            }
        ]

    },
    {
        path: '/about',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/InfoPage/AboutView.vue'),
                name: 'about',
                meta: {
                    title: '關於我們'
                }
            }
        ],
    },
    {
        path: '/:catchAll(.*)',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('@/views/HttpCode/NotFound'),
                name: 'NotFound',
                meta: {
                    title: '找不到網頁'
                }
            }
        ],
    },


]
const routes = [...IndexRoute, ...UserRoute, ...StoreRoute, ...OrderRoute];
const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
    scrollBehavior() {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve({left: 0, top: 0})
            }, 0)
        })
    },
})


router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth && !store.getters.isSignIn) {
        next({name: 'loginPage', query: {redirectUrl: to.fullPath}});
    } else {
        window.document.title = to.meta.title;
        next();
    }
})

export default router
