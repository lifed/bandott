import LayoutIndex from "@/Layout/index.vue";

export const OrderRoute = [
    {
        path: '/store/:storeId/',
        component: LayoutIndex,
        children: [
            {
                path: 'addOrder',
                name: 'addOrder',
                component: () => import('../views/Order/./OrderEditor'),
                meta: {
                    title: '新增訂單',
                    requiresAuth: true
                }
            },
            {
                path: 'editOrder',
                name: 'editOrder',
                component: () => import('../views/Order/./OrderEditor'),
                meta: {
                    title: '修改訂單',
                    requiresAuth: true
                }
            },
            {
                path: 'viewOrder',
                name: 'viewOrder',
                component: () => import('../views/Order/./OrderEditor'),
                meta: {
                    title: '檢視訂單',
                    requiresAuth: true
                }
            },
        ]
    },
    {
        path: '/orders',
        component: LayoutIndex,
        children: [
            {
                path: 'tables',
                component: () => import('../views/Order/OrderList.vue'),
                name: 'tables',
                meta: {
                    title: '找桌',
                    requiresAuth: false
                }
            },
            {
                path: 'published',
                component: () => import('../views/Order/OrderList.vue'),
                name: 'published',
                meta: {
                    title: '我的訂單',
                    requiresAuth: true
                }
            },
            {
                path: 'joined',
                component: () => import('../views/Order/OrderList.vue'),
                name: 'joined',
                meta: {
                    title: '參加訂單',
                    requiresAuth: true
                }
            }
        ],
    },
    {
        path: '/tableList',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/Order/OrderList.vue'),
                name: 'tableList',
                meta: {
                    title: '找桌'
                }
            }
        ],
    },
]