import LayoutIndex from "@/Layout/index.vue";

export const UserRoute = [
    {
        path: '/register',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/MemberPage/RegisterView.vue'),
                name: 'Register',
                meta: {
                    title: '註冊會員'
                    , showHeader: false
                }
            }
        ],
    },
    {
        path: '/login',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/LoginView'),
                name: 'loginPage',
                meta: {
                    title: '登入',
                    showHeader: false
                }
            }
        ],
    },
    {
        path: '/logout',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/LogoutView'),
                name: 'logoutPage',
                meta: {
                    title: '登出',
                    showHeader: false,
                    // requiresAuth: true
                }
            }
        ],
    },
    {
        path: '/settings',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/MemberPage/MemSetting'),
                name: 'UserSettings',
                meta: {
                    title: '設定'
                    , showHeader: false
                    , requiresAuth: true
                }
            }
        ],
    },
    {
        path: '/forgetpassword',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/MemberPage/SendPasswordResetEmailView.vue'),
                name: 'forgetPwd',
                meta: {
                    title: '忘記密碼'
                    , showHeader: false
                }
            }
        ],
    },
    {
        path: '/confirmedResetPassword',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/MemberPage/ConfirmedResetPassword.vue'),
                name: '修改密碼',
                meta: {
                    title: '修改密碼'
                }
            },

        ],
    },
]