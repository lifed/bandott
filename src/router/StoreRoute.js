import LayoutIndex from "@/Layout/index.vue";

export const StoreRoute = [
    {
        path: '/stores',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/Store/StoreView.vue'),
                name: 'storeList',
                meta: {
                    title: '開桌'
                }
            },
        ],
    },
    {
        path: '/store/:storeId/menu',
        component: LayoutIndex,
        children: [
            {
                path: '',
                component: () => import('../views/Store/StoreMenu.vue'),
                name: 'storeMenu',
                meta: {
                    title: '菜單'
                }
            },
        ],
    },

]