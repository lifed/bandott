import axiosInstance from './HttpClient'

const orderReq = axiosInstance

const prefixNonPublic = '/order'

export const getJoinedOrderDetails = (orderId) => orderReq.get(prefixNonPublic + '/' + orderId + '/JoinedDetail')
export const getPublishedOrderDetails = (orderId) => orderReq.get(prefixNonPublic + '/' + orderId + '/PublishedDetail')
export const addOrderDetail = (orderId, data) => orderReq.post(prefixNonPublic + '/' + orderId + '/detail', data)
export const deleteOrderDetail = (orderId, orderDetailId) => orderReq.delete(prefixNonPublic + '/' + orderId + '/detail/' + orderDetailId)
export const patchOrderDetail = (orderId, orderDetailId, data) => orderReq.patch(prefixNonPublic + '/' + orderId + '/detail/' + orderDetailId, data)
export const patchDetailMealStatusByUser = (orderId, userId) => orderReq.patch(prefixNonPublic + '/' + orderId + '/detail/batch/Paid/' + userId)
export const patchDetailMealStatus = (orderId) => orderReq.patch(prefixNonPublic + '/' + orderId + '/detail/batch/Reserved')
export const getOrderPick = (orderId) => orderReq.get(prefixNonPublic + '/' + orderId + '/PickupDetail')

