import axiosInstance from './HttpClient'

const storeReq = axiosInstance

const prefixPublic = '/store/p'
const prefixNonPublic = '/store'

export const getStore = (storeId) => storeReq.get(prefixPublic + '/storeInfo/' + storeId)
export const getStores = (page = 1, data) => storeReq.get(prefixPublic + '/stores/' + page, {params: data})
export const getStoreSuggestionList = (keyword) => storeReq.get(prefixPublic + '/suggestionList/' + keyword)
export const getStoreMenu = (storeId) => storeReq.get(prefixPublic + '/storeDetail/' + storeId)
export const getStoreEditor = (storeId) => {
    if (storeId) {
        return storeReq.get(prefixNonPublic + '/editor/' + storeId)
    } else {
        return storeReq.get(prefixNonPublic + '/editor')
    }
}
export const addStore = (data) => storeReq.post(prefixNonPublic, data)
export const editStore = (storeId, data) => storeReq.patch(prefixNonPublic + '/' + storeId, data)
export const getStoreCategoriesByUser = () => storeReq.get(prefixPublic + '/storeCategories')
export const getStoreCategories = () => storeReq.get(prefixNonPublic + '/storeCategories')
export const modifyCategories = (data) => storeReq.post(prefixNonPublic + '/storeCategories', data)

export const getMenuEditor = (storeId, menuId) => {
    if (menuId) {
        return storeReq.get(prefixNonPublic + '/' + storeId + '/editor/' + menuId)
    } else {
        return storeReq.get(prefixNonPublic + '/' + storeId + '/editor')
    }
}
export const addStoreMenu = (storeId, data) => storeReq.post(prefixNonPublic + '/' + storeId + '/menu', data)
export const editStoreMenu = (storeId, menuId, data) => storeReq.patch(prefixNonPublic + '/' + storeId + '/menu/' + menuId, data)
export const removeStoreMenu = (storeId, menuId) => storeReq.delete(prefixNonPublic + '/' + storeId + '/menu/' + menuId)
export const getStoreMenuCategories = (storeId) => storeReq.get(prefixNonPublic + '/' + storeId + '/menuCategories')
export const modifyMenuCategories = (storeId, data) => storeReq.post(prefixNonPublic + '/' + storeId + '/menuCategories', data)
export const removeStoreImage = (storeId, imageId, imageType) => storeReq.delete(prefixNonPublic + '/file/' + storeId + '/' + imageId + '/' + imageType);
