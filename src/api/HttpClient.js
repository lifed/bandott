import axios from "axios"
import router from "@/router"
import store from '@/store'
import {globalVars} from "@/properties";

class HttpClient {
    constructor() {
        this.instance = axios.create({
            baseURL: 'https://f3530fb2d46f.sn.mynetname.net/api'
            // baseURL: 'http://localhost:8082/api'
            , withCredentials: true
            // , xsrfCookieName: 'XSRF-TOKEN'
            // , xsrfHeaderName: 'X-XSRF-TOKEN'
        });
        this.setInterceptors();
    }

    setInterceptors() {
        this.instance.interceptors.response.use(
            (response) => {
                return response
            },
            (error) => {
                if (error.response.status === 400) {
                    if (error.response.data && error.response.data.ExceptionMessage) {
                        alert(error.response.data.ExceptionMessage)
                    }
                    console.log('請求失敗。')
                } else if (error.response.status === 401) {
                    console.log('未驗證。')
                    router.push({name: 'loginPage', query: {redirectUrl: error.response.config.url}})
                } else if (error.response.status === 404) {
                    console.log('找不到頁面。')
                } else if (error.response.status === 500) {
                    console.log('請求頁面異常，請聯絡管理員。')
                } else {
                    console.log('未知錯誤。')
                }
                // return Promise.reject(error);
            }
        )
        this.instance.interceptors.request.use(
            async (config) => {
                if (globalVars.loginMode === 'session' &&
                    (config.method.toLowerCase() === 'post' ||
                        config.method.toLowerCase() === 'patch' ||
                        config.method.toLowerCase() === 'delete')) {
                    await this.instance.get("/server/p/csrf").then((response) => {
                        if (response.status === 200 && response.data) {
                            config.headers[response.data.headerName] = response.data.token;
                            // 注意！用下方設定會導致第一次請求無法正確新增Token。
                            // this.instance.defaults.headers.common[response.data.headerName] = response.data.token;
                        }
                    })
                }
                if (globalVars.loginMode === 'jwt') {
                    config.headers['Authorization'] = 'Bearer ' + store.getters.getUserToken;
                }
                config.timeout = 5000
                return config
            },
            error => {
                console.log(error)
            }
        )
    }

    getAxios() {
        return this.instance
    }
}

const axiosInstance = new HttpClient().getAxios()
export default axiosInstance


