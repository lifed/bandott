import axiosInstance from './HttpClient'

const userReq = axiosInstance
const prefixPublic = '/member/p'
const prefixNonPublic = '/member'

export const isServerOn = (callback) => {
    userReq.get('/server/p/isAlive', {timeout: 1000})
        .then(res => {
            callback(res.status === 200);
        })
        .catch(() => {
            callback(false);
        });
}
export const signUp = data => userReq.post(prefixPublic + '/signUp', data)
export const signIn = data => userReq.post(prefixPublic + '/signIn/processing', data)
export const jwtSignIn = data => userReq.post(prefixPublic + '/jwtSignIn', data)
export const signOut = data => userReq.post(prefixPublic + '/signOut', data, {timeout: 1000})
export const jwtSignOut = data => userReq.post(prefixPublic + '/jwtSignOut', data, {timeout: 1000})
export const sendPasswordResetEmail = data => userReq.post(prefixPublic + '/sendPasswordResetEmail', data)
export const verifyPasswordResetCode = data => userReq.post(prefixPublic + '/verifyPasswordResetCode', data)
export const updatePasswordWithCode = data => userReq.post(prefixPublic + '/updatePasswordWithCode', data)
export const getUserEditableInfo = data => userReq.get(prefixNonPublic + '/getUserEditableInfo', data)
export const updateUserEditableInfo = data => userReq.post(prefixNonPublic + '/updateUserEditableInfo', data)