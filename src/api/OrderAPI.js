import axiosInstance from './HttpClient'

const orderReq = axiosInstance

const prefixPublic = '/order/p'
const prefixNonPublic = '/order'

export const addOrder = (data) => orderReq.post(prefixNonPublic + '/editorOrder', data)
export const getPublishedOrder = (page = 1) => orderReq.get(prefixNonPublic + '/published/' + page)
export const getOrder = (orderId) => orderReq.get(prefixNonPublic + '/getOrder/' + orderId)
export const editOrder = (orderId, data) => orderReq.patch(prefixNonPublic + '/editorOrder/' + orderId, data)
export const deleteOrder = (orderId) => orderReq.delete(prefixNonPublic + '/published/' + orderId)
export const getTables = (page = 1) => orderReq.get(prefixPublic + '/tables/' + page)
export const getJoinedOrders = (page = 1) => orderReq.get(prefixNonPublic + '/joined/' + page)
