export const disabledInputs = () => {
    var inputs = document.getElementsByTagName("input");

    for (var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = true;
    }
}

export const disabledSelects = () => {
    var selects = document.getElementsByTagName("select");

    for (var i = 0; i < selects.length; i++) {
        selects[i].disabled = true;
    }
}

export const disabledAll = () => {
    disabledInputs()
    disabledSelects()
}