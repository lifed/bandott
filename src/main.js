import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import '@/assets/scss/bootstrap.scss'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'animate.css'

import '@/assets/css/style.css'
import BounceLoader from 'vue-spinner/src/BounceLoader'
import LoadingButton from './components/LoadingButton.vue'

createApp(App)
    .component('BounceLoader',BounceLoader)
    .component('LoadingButton', LoadingButton)
    .use(store)
    .use(router)
    .mount('#app')
