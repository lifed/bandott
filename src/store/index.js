import {createStore} from 'vuex'
import userInfo from '@/store/UserInfo'
// import createPersistedState from "vuex-plugin-persistedstate"
import createPersistedState from "vuex-persistedstate"
export default createStore({
    state() {
        return {
            serverStatus: false
        }
    },
    mutations: {
        setServerStatus (state, payload) {
            state.serverStatus = payload
        }
    },
    actions: {},
    modules: {
        userInfo
    },
    plugins: [
        createPersistedState()
    ]
})
