const state = {
    user: {
        token: '',
        id: '',
        picURL: '',
        name: '',
        nickname:'',
        auth: '',
        mode: '',
    },
};

const getters = {
    isSignIn: (state) => state.user.token !== '',
    isEditor: (state) => state.user.auth === 'backend',
    isMode: (state) => state.user.mode === true,
    getUserToken: (state) => state.user.token,
    getUserPicUrl: (state) => state.user.picURL,
};

const mutations = {
    setUserInfo(state, userInfo) {
        // 若userInfo的key無對應會被append.
        state.user = {...state.user, ...userInfo,}
    },
    updateUserInfo(state, userInfo) {
        state.user.picURL = userInfo.userPictureURL
        state.user.name = userInfo.name
        state.user.nickname = userInfo.nickname
    },
    setMode(state, mode) {
        state.user.mode = mode
    },
    logout(state) {
        Object.keys(state.user).forEach(key => {
            state.user[key] = '';
        });
    },
};

const actions = {
    async setMode({commit}, mode) {
        await commit('setMode', mode);
    },
    async login({commit}, userInfo) {
        await commit('setUserInfo', userInfo);
    },
    async updateUserInfo({commit}, userInfo) {
        await commit('updateUserInfo', userInfo);
    },
    async logout({commit}) {
        await commit('logout');
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};