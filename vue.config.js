const {defineConfig} = require('@vue/cli-service')

// 設定檔
const finalConfig = {}
// 預設設定
let defaultConfig = {"transpileDependencies": true}
// 不設定該參數(設了會有問題)
// let publicPath = ''
// let devServerConfig = {historyApiFallback: false}
// 線上設定
let onlineConfig = {}
if (process.env.NODE_ENV === 'production') {
    onlineConfig = {
        "publicPath": "/bandott/",
        "devServer": {historyApiFallback: true},
    }
}
// 合併設定檔
for (let key in defaultConfig) {
    finalConfig[key] = defaultConfig[key]
}
for (let key in onlineConfig) {
    finalConfig[key] = onlineConfig[key]
}
module.exports = defineConfig(finalConfig)
